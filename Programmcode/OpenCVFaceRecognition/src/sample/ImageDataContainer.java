package sample;

/*  OpenCV Imports  */
import org.bytedeco.javacpp.opencv_core.Mat;

/*  Sonstige Imports  */
import java.util.ArrayList;

/**
 * Datei zum Speichern von Bildern und ihren Metadaten, geordnet nach Personen ID.
 * Created by Frederick on 03.09.2016.
 */
public class ImageDataContainer {

    // Array aller Bilder der Person
    private ArrayList<Mat> images;
    // Dateinamen aller Bilder der Person
    private ArrayList<String> filenames;
    // ID der Person
    private Integer id;

    /**
     * Konstruktor des ImageDataContainers.
     * @param image
     *      Initiales Bild der Person
     * @param filename
     *      Dateiname des initialen Bilds der Person.
     * @param id
     *      Einzigartige ID der Person.
     */
    public ImageDataContainer(Mat image, String filename, Integer id) {
        images = new ArrayList<>();
        images.add(image);

        filenames = new ArrayList<>();
        filenames.add(filename);

        this.id = id;
    }

    /**
     * Methode zum Hinzufuegen eines Bildes zum Container.
     * @param image
     *      Bild als Mat.
     * @param filename
     *      Dateiname des Bildes.
     */
    public void add(Mat image, String filename) {
        images.add(image);
        filenames.add(filename);
    }

    /**
     * Methode zum Entfernen eines Bildes aus dem Container.
     * @param image
     *      Das Bild.
     * @return
     *      True, wenn das Bild erfolgreich entfernt wurde.
     */
    public boolean remove(Mat image) {
        if (images.contains(image)) {
            int index = images.indexOf(image);
            images.remove(index);
            filenames.remove(index);
            return true;
        }
        return false;
    }

    /**
     * Methode zum Entfernen eines Bildes aus dem Container.
     * @param filename
     *      Dateiname des Bildes.
     * @return
     *      True, wenn das Bild erfolgreich entfernt wurde.
     */
    public boolean remove(String filename) {
        if (filenames.contains(filename)) {
            remove(images.get(filenames.indexOf(filename)));
            return true;
        }
        return false;
    }

    /**
     * Getter fuer den Bild-Array des Containers.
     * @return
     *      ArrayList mit allen Mats.
     */
    public ArrayList<Mat> getImages() {
        return images;
    }

    /**
     * Getter fuer die Anzahl der Bilder im Container.
     * @return
     *      Anzahl der Bilder im Container
     */
    public int getNumberOfImages() {
        return images.size();
    }

    /**
     * Getter fuer den Filename-Array des Containers.
     * @return
     *      ArrayList mit allen Strings.
     */
    public ArrayList<String> getFilenameList() {
        return filenames;
    }

    /**
     * Getter fuer die ID des Containers.
     * @return
     *      Die ID des Containers als Integer.
     */
    public Integer getID() {
        return id;
    }
}

