package sample;

/*  JavaFX Imports  */
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Main Klasse der OpenCVFaceRecognition Applikation.
 */
public class Main extends Application {
    /**
     * Start Methode der JavaFX GUI.
     * @param primaryStage
     *      Die primaryStage der GUI.
     */
    @Override
    public void start(Stage primaryStage) {
        try
        {
            // Lade den FXML File fuer die GUI
            FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
            BorderPane root = loader.load();

            // Erstelle die Scene (das Fenster)
            Scene scene = new Scene(root);

            // Erstelle die Stage
            primaryStage.setTitle("Face Detection");
            primaryStage.setScene(scene);
            // GUI anzeigen
            primaryStage.show();

            // init the controller
            sample.Controller controller = loader.getController();
            controller.init(primaryStage);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Main Methode.
     * @param args
     *      Kommandozeilenargumente.
     */
    public static void main(String[] args)
    {
        launch(args);
    }
}
