package sample;

/*  JavaFX Imports  */
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/*  OpenCV Imports  */
import org.bytedeco.javacpp.opencv_core.*;
import org.bytedeco.javacpp.opencv_imgcodecs;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;

/*  Sonstige Imports  */
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.io.FileUtils;

/**
 * Klasse des JavaFX Controllers. Verwaltet GUI-Elemente und deren Funktionen.
 */
public class Controller {

    // Unterstuetzte Bildformate
    //      jpg - grey FERET thumbnails
    //      ppm - color FERET
    //      tif - grey FERET high-resolution
    //      bz2 - komprimierte Bilder (braeuchte extra Logik)
    //      png - von diesem Programm ausgegebene Bilder (CROPPED)
    private static String[] imageExtensions = { /*"ppm", "ppm.bz2", "tif", "tif.bz2",*/ "jpg", "png"};
    // File Chooser zum Auswählen von Ordnern.
    private DirectoryChooser directoryChooser;
    // Stage des Hauptfensters.
    private Stage stage;
    // ImageProcessor zur Bildverarbeitung.
    private ImageProcessor imageProcessor;
    // Image Manager zum Verwalten der Trainingsbilder.
    private ImageManager trainingImageManager;
    // Image Manager zum Verwalten der Testbilder.
    private ImageManager testingImageManager;
    // Ordner, aus dem die Trainingsbilder eingelesen werden.
    private File trainingDir;
    // Separater Task fuer die Gesichtserkennung.
    private Task recognizer;


    /* *****************
     *  Progress Bars  *
     *******************/

    // ProgressBar fuer die Face Recognition
    @FXML
    private ProgressBar recognitionProgress;


    /* *************
     *  ListViews  *
     ***************/

    // Liste der Trainingsbilder
    @FXML
    private ListView<String> trainingImageListView;
    // Liste der Testbilder
    @FXML
    private ListView<String> testingImageListView;


    /* **********************
     *  Labels + Textboxen  *
     ********************** */

    // Label zur Angabe des FaceRecognizer training Status.
    @FXML
    private Label trainingStatusLabel;
    // Label mit der ID des in der GUI angewaehlten trainings-Gesichts
    @FXML
    private Label faceIDLabelTrain;
    // Label mit der ID des in der GUI angewaehlten test-Gesichts
    @FXML
    private Label faceIDLabelTest;
    // Label fuer Anzahl der Personen auf die die angegebenen Einschraenkungen zutreffen
    @FXML
    private Label validSetsLabel;
    // Label fuer das Ergebnis der letzten image Recognition.
    @FXML
    private Label recognitionResult;


    /* ***********
     *  Buttons  *
     *********** */

    // Button zum Starten der Gesichtserkennung
    @FXML
    private Button recognizeButton;
    // Button zum Trainieren des Recognizers
    @FXML
    private Button trainButton;
    // Button zum Speichern der bereits gecroppten Images
    @FXML
    private Button saveImagesButton;
    // Button der den Dateibrowser fuer Trainingsbilder oeffnet
    @FXML
    private Button loadButtonLeft;
    // Button der den Dateibrowser fuer Testbilder oeffnet
    @FXML
    private Button loadButtonRight;
    // Button der aktuell selektierte Bild aus der Trainingsbilderliste loescht
    @FXML
    private Button deleteButton;

    // Radiobutton zum Waehlen des Fisher Face Recognizers.
    @FXML
    private RadioButton fisherSelected;
    // Radiobutton zum Waehlen des Eigen Face Recognizers.
    @FXML
    private RadioButton eigenSelected;
    // Radiobutton zum Waehlen des Local Binary Pattern Recognizers.
    @FXML
    private RadioButton lbpSelected;


    /* ***************
     *  Image Views  *
     *************** */

    // Preview fuer Bild aus der Trainingsbilderliste
    @FXML
    private ImageView leftImageView;
    // Preview fuer zu erkenndes Bild
    @FXML
    private ImageView rightImageView;


    /* ***************
     *  Combo Boxes  *
     *************** */

    // Combo Box fuer das Auswahl der Anzahl der Bilder pro Person fuer das Training
    @FXML
    private ComboBox facesPerPersonComboBox;


    /* ***************
     *  Check Boxes  *
     *************** */

    // Checkbox die angibt ob Bilder beim Oeffnen erst zugeschnitten werden sollen
    @FXML
    private CheckBox cropOnOpen;

    // Checkbox fur Aktivierung der Zeitmessung
    @FXML
    private CheckBox measureTimes;


    /* *******************
     *  Action Methoden  *
     ******************* */

    /**
     * Methode zum Loeschen des Selektierten Items aus der Trainingsbild Liste.
     */
    @FXML
    public void onListItemDelete() {
        String selectedImageFilename = trainingImageListView.getSelectionModel().getSelectedItem();
        if (selectedImageFilename != null) {
            trainingImageManager.delete(selectedImageFilename);
            trainingImageListView.setItems(trainingImageManager.getFilenameList());
            setComboboxValues();
        }
    }

    /**
     * Methode zum Oeffnen des Directory Choosers fuer das Waehlen des Basisordners der (FERET) Bilddateien.
     */
    @FXML
    public void onTrainingImageOpen() {
        directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select FERET Base Directory");

        // Array zum Speichern der je Bild gefundenen Gesichter
        ArrayList<Mat> croppedFaces;

        boolean imageSizeError = false;
        int rows = 0;
        int cols = 0;

        // Oeffne den Directory Chooser
        trainingDir = directoryChooser.showDialog(stage);

        // Wenn eine Directory gewaehlt wurde...
        if (trainingDir != null) {
            // Suche alle Bilddateien
            Collection<File> fileList = FileUtils.listFiles(trainingDir, imageExtensions/*null*/, true);
            trainingImageManager.clear();
            if (fileList.size() > 0) {
                // Lese alle Bilddateien
                for (File imageFile : fileList) {
                    // Bild einlesen und in Graustufen konvertieren
                    Mat image = imread(imageFile.getAbsolutePath(), opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                    String filename = imageFile.getName();
                    String id = filename.substring(0, 5);
                    // Wenn "Crop On Open" selektiert ist, schneide die Bilder zu
                    if (cropOnOpen.isSelected()) {

                        long timeStart = System.currentTimeMillis();

                        // Gesicht aus Bild extrahieren
                        croppedFaces = imageProcessor.cropFaces(image);

                        if (measureTimes.isSelected()) {
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Face Crop Time");
                            alert.setHeaderText(null);
                            alert.setContentText("It took\n" + (System.currentTimeMillis()-timeStart) + "ms\nto crop"
                            + croppedFaces.size() + " faces.");
                            alert.show();
                        }

                        // Wenn ein Gesicht gefunden wurde
                        if (croppedFaces.size() > 0 && croppedFaces.get(0) != null) {
                            Mat face = croppedFaces.get(0);
                            // Im ImageManager speichern
                            trainingImageManager.add(face, id, filename);
                        }
                    }
                    // Wenn "Crop On Open" Nicht selektiert wurde
                    // pruefe Bilddimensionen
                    else {
                        if (rows == 0 && cols == 0) {
                            rows = image.rows();
                            cols = image.cols();
                        } else if (rows != image.rows() || cols != image.cols()) {
                            imageSizeError = true;
                        }
                        // Im ImageManager speichern
                        trainingImageManager.add(image, id, filename);
                    }
                }
                // Error-Notification: Falsche Bildgroesse
                if (imageSizeError) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Image Size Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Not all faces have the same dimensions." +
                            "\nCheck \"Crop On Open\" and reload images or the recognizer will most likely crash!"
                    );
                    alert.show();
                }

                // Aktiviere Buttons, aktualisiere Combobox und fuelle Bildliste
                saveImagesButton.setDisable(false);
                recognizeButton.setDisable(true);
                trainingImageListView.setItems(trainingImageManager.getFilenameList());
                setComboboxValues(); // combobox anpassen
            }
            // Error-Notification: Keine Bilder gefunden
            else {
                displayNoImageFoundMessage();
            }
        }
        // Error-Notification: Keine Directory ausgewaehlt
        else {
            displayDirErrorMessage();
        }

    }

    /**
     * Methode die beim Auswaehlen eines Wertes mit der Combobox aufgerufen wird.
     * Aendert das Label der Anzahl der Personen, welche den Einschraenkungen der Combobox entsprechen.
     */
    @FXML
    public void onComboboxSelected() {
        validSetsLabel.setText(""+ trainingImageManager.getAmountOfPeopleWithMorePicturesThan(getComboboxValue()));
    }

    /**
     *  Methode zum oeffnen eines File Choosers zum Laden des zu Bildes in dem ein Gesicht erkannt werden soll.
     */
    @FXML
    public void onRecognitionImageOpen() {
        directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select Test Image Base Directory");

        // Oeffne den Directory Chooser
        File testingDir = directoryChooser.showDialog(stage);

        // Wenn eine Directory gewaehlt wurde...
        if (testingDir != null) {
            // Suche alle Bilddateien
            Collection<File> fileList = FileUtils.listFiles(testingDir, imageExtensions, true);
            testingImageManager.clear();
            if (fileList.size() > 0) {
                // Lese alle Bilddateien
                for (File imageFile : fileList) {
                    // Bild einlesen und in Graustufen konvertieren
                    Mat image = imread(imageFile.getAbsolutePath(), opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                    String filename = imageFile.getName();
                    String id = filename.substring(0, 5);
                    // Im ImageManager speichern
                    testingImageManager.add(image, id, filename);
                }
                testingImageListView.setItems(testingImageManager.getFilenameList());
                if (trainingImageManager.getData().size() > 0 && imageProcessor.isTrained())
                    recognizeButton.setDisable(false);
            }
            // Error-Notification: Keine Bilder gefunden
            else {
                displayNoImageFoundMessage();
            }
        }
        // Error-Notification: Keine Directory ausgewaehlt
        else {
            displayDirErrorMessage();
        }
    }

    /**
     * Methode welche alle geladenen Bilder im Unterordner "cropped" speicher.
     * Nuetzlich, um zum Beispiel gecroppte Images fuer spaeteren Gebrauch zu speichern.
     */
    @FXML
    public void onSaveImagesClicked () {
        File croppedFolder = new File(trainingDir.getAbsolutePath()+"/cropped");
        if (!croppedFolder.mkdir()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Image Save Error");
            alert.setHeaderText(null);
            alert.setContentText("An Error occured while creating the directory '/cropped'");
            alert.show();
        } else {
            for (ImageDataContainer container : trainingImageManager.getData()) {
                for (int i = 0; i < container.getNumberOfImages(); i++) {
                    Mat mat = container.getImages().get(i);
                    String formattedID = String.format("%05d", container.getID());
                    String filename = formattedID + "_" + i + "_CROPPED" + ".png";
                    BufferedImage bImage = SwingFXUtils.fromFXImage(Utils.mat2Image(mat), null);
                    try {
                        ImageIO.write(bImage, "png", new File(croppedFolder.getAbsoluteFile() + "/" + filename));
                    } catch (Exception e) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Image Save Error");
                        alert.setHeaderText(null);
                        alert.setContentText("An Error occured when saving image " + filename);
                        alert.show();
                    }
                }
            }
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Images Saved");
            alert.setHeaderText(null);
            alert.setContentText("All images have been saved successfully.");
            alert.show();
        }
    }

    /**
     * Methode die beim Anklicken des "Train Recognizer" Buttons aufgerufen wird.
     * Trainiert den Face Recognizer mit den geladenen Trainingsbildern.
     */
    @FXML
    public void onTrainClicked () {
        long timeStart = System.currentTimeMillis();
        if (imageProcessor.train(trainingImageManager.getData(), getComboboxValue())) {

            if (measureTimes.isSelected()) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Training Time");
                alert.setHeaderText(null);
                alert.setContentText("It took\n" + (System.currentTimeMillis()-timeStart) + "ms\nto train");
                alert.show();
            }

            trainingStatusLabel.setText("Trained");
            recognizeButton.setDisable(false);

            trainingImageListView.setItems(trainingImageManager.getFilenameList(getComboboxValue()));
        }
    }

    /**
     * Methode die beim Anklicken des "Recognize" Buttons aufgerufen wird.
     * Erkennt das geladene Testbild mit dem trainierten Face Recognizer.
     */
    @FXML
    public void onRecognizeClicked () {
        setGlobalDisableState(true);

        recognitionResult.setText("Pending...");

        // Neuen Recognizer in Thread/Task erstellen
        recognizer = createRecognizer();
        recognizer.setOnSucceeded(event -> {
            displayRecognitionResult((String)recognizer.getValue());
            setGlobalDisableState(false);
        });

        // ProgressBar Wert an Task Completion binden
        recognitionProgress.progressProperty().unbind();
        recognitionProgress.progressProperty().bind(recognizer.progressProperty());

        // Task starten
        new Thread(recognizer).start();
    }

    /**
     * Methode, welche FisherFaces als Recognizer Art einstellt.
     */
    @FXML
    public void onFisherSelected () {
        eigenSelected.setSelected(false);
        lbpSelected.setSelected(false);
        recognizeButton.setDisable(true);
        trainButton.setDisable(false);

        imageProcessor.setFisherRecognizer();
    }

    /**
     * Methode, welche EigenFaces als Recognizer Art einstellt.
     */
    @FXML
    public void onEigenSelected () {
        fisherSelected.setSelected(false);
        lbpSelected.setSelected(false);
        recognizeButton.setDisable(true);
        trainButton.setDisable(false);

        imageProcessor.setEigenRecognizer();
    }

    /**
     * Methode, welche LocalBinaryPattern als Recognizer Art einstellt.
     */
    @FXML
    public void onLBPSelected () {
        eigenSelected.setSelected(false);
        fisherSelected.setSelected(false);
        recognizeButton.setDisable(true);
        trainButton.setDisable(false);

        imageProcessor.setLBPRecognizer();
    }


    /* *******************
     *  Andere Methoden  *
     ******************* */

    /**
     * Methode zum Erstellen eines neuen Recognizers in einem eigenen Thread/Task.
     * Noetig, um im separaten GUI-Thread die Progress-Bar aktualisieren zu koennen.
     * @return
     *      Der Thread.
     */
    public Task createRecognizer() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                ArrayList<PredictionOutcome> recognitionResults = new ArrayList<>();
                double recognitionPercentage;
                double totalCorrDistance = 0.0;
                double avgCorrDistance;
                double lowestCorrDistance = Double.MAX_VALUE;
                double highestCorrDistance = -Double.MAX_VALUE;
                double totalDistance = 0.0;
                double avgDistance;
                double lowestDistance = Double.MAX_VALUE;
                double highestDistance = -Double.MAX_VALUE;
                int totalTested = 0;
                int testedCorrectly = 0;
                int rejectedCorrectly = 0;
                int falsePositives = 0;
                //int noFaceFound = 0;
                int correctID;
                int totalAmount = testingImageManager.getTotalAmountOfPictures(getComboboxValue());

                long timeStart = System.currentTimeMillis();

                // Gehe alle Testpersonen durch
                for (ImageDataContainer container : testingImageManager.getData()) {
                    correctID = container.getID();
                    // Gehe alle Bilder der Testperson durch
                    for (Mat image : container.getImages()) {
                        totalTested++;

                        PredictionOutcome outcome = imageProcessor.recognize(image, 1);
                        outcome.setCorrectLabel(correctID);
                        recognitionResults.add(outcome);
                        updateProgress(totalTested, totalAmount);
                    }
                }
                
                // Ergebnisverarbeitung
                for (PredictionOutcome outcome : recognitionResults) {
                    totalDistance += outcome.getPredictedDistance();
                    if (outcome.getPredictedDistance() < lowestDistance && outcome.getPredictedDistance() >= 0)
                        lowestDistance = outcome.getPredictedDistance();
                    if (outcome.getPredictedDistance() > highestDistance)
                        highestDistance = outcome.getPredictedDistance();

                    // Pruefe ob sie korrekt erkannt wird:

                    // Wenn Label richtig ist und Konfidenz hoch genug: Person korrekt erkannt
                    if (outcome.isCorrect()) {
                        testedCorrectly++;
                        totalCorrDistance += outcome.getPredictedDistance();
                        if (outcome.getPredictedDistance() < lowestCorrDistance)
                            lowestCorrDistance = outcome.getPredictedDistance();
                        if (outcome.getPredictedDistance() > highestCorrDistance)
                            highestCorrDistance = outcome.getPredictedDistance();
                    }
                    // -2 ist "No face found"-Error-State aus ImageProcessor
                    // d.h. kein Gesicht im Testbild erkannt.
                    //else if (outcome.getPredictedLabel() == -2) {
                    //    noFaceFound++;
                    //}
                    // Wenn Label falsch ist und Konfidenz trotzdem hoch: false Positive
                    else if (!outcome.isCorrect() && outcome.getPredictedLabel() != -1) {
                        falsePositives++;
                    }
                    // Wenn Person korrekt als nicht im Trainingsdatensatz enthalten erkanntr wird
                    else if (outcome.getPredictedLabel() == -1
                            && !imageProcessor.getTrainedIDs().contains(outcome.getCorrectLabel())) {
                        rejectedCorrectly++;
                    }
                }

                // Errechne die Erkennungsrate
                recognitionPercentage = (double) testedCorrectly / (double) totalTested;
                avgCorrDistance = totalCorrDistance / testedCorrectly;
                avgDistance = totalDistance / totalTested;

                return    "\nRecognized correctly (%): " + MessageFormat.format("{0,number,#.##%}", recognitionPercentage)
                        + "\nRecognized correctly (#): " + testedCorrectly
                        + "\nTime: " + (System.currentTimeMillis() - timeStart) + "ms"
                        + "\nRejected Correctly: " + rejectedCorrectly
                        + "\nTotal tested: " + totalTested
                        + "\nFalse positives: " + falsePositives
                        + "\nAvg. distance (when correct): " + (int) avgDistance + "  (" + (int) avgCorrDistance + ")"
                        + "\nLowest distance (when correct): " + (int) lowestDistance + "  (" + (int) lowestCorrDistance + ")"
                        + "\nHighest distance (when correct): " + (int) highestDistance + "  (" + (int) highestCorrDistance + ")";
            }
        };
    }

    /**
     * Methode zum Aktivieren und Deaktivieren aller Steuerelemente.
     * @param disabled
     *      True fuer alle deaktivieren, false fuer alle aktivieren
     */
    public void setGlobalDisableState(boolean disabled) {
        recognizeButton.setDisable(disabled);
        trainButton.setDisable(disabled);
        saveImagesButton.setDisable(disabled);
        loadButtonLeft.setDisable(disabled);
        loadButtonRight.setDisable(disabled);
        deleteButton.setDisable(disabled);
        facesPerPersonComboBox.setDisable(disabled);
    }

    /**
     * Methode zum Darstellen der "Kein Ordner angegeben"-Fehlermeldung.
     */
    private void displayDirErrorMessage() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Open Directory Error");
        alert.setHeaderText(null);
        alert.setContentText("Error opening directory.");
        alert.show();
    }

    /**
     * Methode zum Darstellen der "Keine Bilder gefunden"-Fehlermeldung.
     */
    private void displayNoImageFoundMessage() {
        String formats = "";
        for (String ext : imageExtensions)
            formats+=ext+" ";

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("No Images Found");
        alert.setHeaderText(null);
        alert.setContentText("No images found in " + trainingDir.getAbsolutePath() + " or it's sub-directories."+
                "\nSupported image formats: " + formats);
        alert.show();
    }

    /**
     * Methode zum Darstellen des Recognition Ergebnis.
     * @param result
     *      Das Ergebnis als String
     */
    private void displayRecognitionResult(String result) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Recognition Test");
        alert.setHeaderText(null);
        alert.setContentText(result);
        alert.show();
        recognitionResult.setText(result);
    }

    /**
     * Methode zum Zurueckgeben des aktuell mit der Combobox selektierten Wertes. 0 bedeutet Max.
     * @return
     *      Der geparste Wert der Combobox.
     */
    private int getComboboxValue() {
        int val;
        try {
            val = Integer.parseInt((String) facesPerPersonComboBox.getValue());
        } catch (NumberFormatException e) {
            val = 0;
        }
        return val;
    }

    /**
     * Methode zum setzen der moeglichen Werte zur Auswahl in der facesPerPersonCombobox.
     * Die ausgewaehlte Anzahl an Gesichtern wird pro Person zum Training genutzt.
     */
    private void setComboboxValues () {
        ArrayList<String> items = new ArrayList<>();

        for (int i = 1; i <= trainingImageManager.getMaxPicturesPerPerson(); i++)
            items.add(""+i);
        items.add("Max");

        facesPerPersonComboBox.getItems().clear();
        facesPerPersonComboBox.getItems().addAll(items);
        facesPerPersonComboBox.getSelectionModel().select(items.get(0));
    }

    /**
     * Init Methode des Contollers.
     * Initialisiert verschiedene Startwerte von Klassenvariablen.
     * @param stage Die Stage
     */
    protected void init(Stage stage)
    {
        imageProcessor = new ImageProcessor(stage);
        trainingImageManager = new ImageManager();
        testingImageManager = new ImageManager();

        // Stage setzen
        this.stage = stage;

        trainingStatusLabel.setText("Untrained");
        recognizeButton.setDisable(true);
        trainButton.setDisable(true);
        saveImagesButton.setDisable(true);

        /* ************
         *  Listener  *
         ************ */

        // Wenn ein List Item angeklickt wird, lade das entsprechende Vorschaubild.
        trainingImageListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && trainingImageManager.getMat(newValue) != null) {
                leftImageView.setImage(Utils.mat2Image(trainingImageManager.getMat(newValue)));
                faceIDLabelTrain.setText(trainingImageManager.getIDofFilenameAsString(newValue));
            }
        });

        testingImageListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && testingImageManager.getMat(newValue) != null) {
                rightImageView.setImage(Utils.mat2Image(testingImageManager.getMat(newValue)));
                faceIDLabelTest.setText(testingImageManager.getIDofFilenameAsString(newValue));
            }
        });
    }
}
