package sample;

/*  OpenCV Imports  */
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;
import org.bytedeco.javacpp.opencv_objdetect;
import org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;
import org.bytedeco.javacpp.opencv_core.*;

import static org.bytedeco.javacpp.opencv_face.createEigenFaceRecognizer;
import static org.bytedeco.javacpp.opencv_face.createFisherFaceRecognizer;
import static org.bytedeco.javacpp.opencv_face.createLBPHFaceRecognizer;
import static org.bytedeco.javacpp.opencv_imgproc.*;

/*  JavaFX Imports  */
import javafx.stage.Stage;

/*  Sonstige Imports  */
import java.util.ArrayList;

/**
 * Klasse fuer Bildverarbeitungsfunktionen zur Gesichtserkennung.
 * Created by Frederick on 12.08.2016.
 */
public class ImageProcessor {

    Stage stage;

    private CascadeClassifier faceClassifier;
    private CascadeClassifier eyeClassifier;

    private FaceRecognizer faceRecognizer;

    private ArrayList<Integer> trainedIDs;
    public ArrayList<Integer> getTrainedIDs() {
        return trainedIDs;
    }

    private boolean isTrained;

    /**
     * Konstruktor des ImageProcessors.
     * @param stage
     *      Die Stage der GUI. Wird benoetigt fuer Fehlermeldungen.
     */
    public ImageProcessor(Stage stage){
        faceClassifier = new CascadeClassifier("res/haarcascades/haarcascade_frontalface_alt.xml");
        eyeClassifier = new CascadeClassifier("res/haarcascades/haarcascade_eye.xml");
        trainedIDs = new ArrayList<>();
        this.stage = stage;
        isTrained = false;
    }

    /**
     * Methode zum Trainieren des FaceRecognizers.
     * @param data
     *      ImageDataContainer Array mit den Daten mit denen der recognizer trainiert werden soll
     * @param trainingImageCount
     *      Mindestanzahl an Bildern mit denen eine Person zum Training uebernommen wird.
     * @return
     *      true wenn Training erfolgreich
     */
    public boolean train(ArrayList<ImageDataContainer> data, int trainingImageCount ) {
        // Wenn kein Recognizer spezifiziert wurde, beende das Training
        if (faceRecognizer == null) return false;

        int maxImages;
        ArrayList<Mat> mats = new ArrayList<>();
        trainedIDs = new ArrayList<>();

        for (ImageDataContainer container : data) {
            // ermittle die Anzahl der fuer das Training zu benutzenden Bilder
            if (trainingImageCount == 0){
                maxImages = container.getNumberOfImages();
            } else if (container.getNumberOfImages() >= trainingImageCount) {
                maxImages = trainingImageCount;
            } else continue;

            // Kopiere Labels (IDs) und Bilder
            for (int i = 0; i < maxImages; i++) {
                mats.add(container.getImages().get(i));
                trainedIDs.add(container.getID());
            }
        }

        // Erstelle einen Vektor aus allen Trainingsbildern
        MatVector faces = new MatVector(mats.size());
        for (int i = 0; i < mats.size(); i++) {
            faces.put(i, mats.get(i));
        }
        Mat labelsMat = new Mat(Utils.intArrayListToArray(trainedIDs));

        // Trainiere die Gesichtserkennung
        faceRecognizer.train(faces, labelsMat);
        isTrained = true;
        return true;
    }

    /**
     * Getter fuer den isTraines Wert.
     * @return
     *      true, wenn der Face Recognizer trainiert ist.
     */
    public boolean isTrained() {
        return isTrained;
    }

    /**
     * Setze den ImageRecognizer Typ auf FisherFace.
     */
    public void setFisherRecognizer () {
        faceRecognizer = createFisherFaceRecognizer();
    }

    /**
     * Setze den ImageRecognizer Typ auf EigenFace.
     */
    public void setEigenRecognizer () {
        faceRecognizer = createEigenFaceRecognizer();
    }

    /**
     * Setze den ImageRecognizer Typ auf LocalBinaryPattern.
     */
    public void setLBPRecognizer () {
        faceRecognizer = createLBPHFaceRecognizer();
    }

    /**
     * Methode zum Erkennen eines eintrainierten Gesichts im Bild image.
     * @param image
     *   Das Bild auf dem die Person erkannt werden soll.
     * @param labelSize
     *   Anzahl an Labels deren Distanz ermittelt werden soll.
     * @return
     *   Label der erkannten Person.
     */
    public PredictionOutcome recognize(Mat image, int labelSize)
    {
        double [] distance = new double[labelSize];
        int [] label = new int[labelSize];

        faceRecognizer.predict(image, label, distance);

        return new PredictionOutcome(label[0], distance[0]);
    }

    /**
     * Methode zum Extrahieren des Gesichts aus einem Bild. Bei mehreren Gesichtern wird nur das erste extrahiert.
     * @param image
     *   Bild, aus dem das Gesicht extrahiert werden soll.
     * @return
     *   Das erste im Bild gefundene Gesicht.
     */
    public ArrayList<Mat> cropFaces(Mat image) {
        RectVector faces = new RectVector(); // alle gefundenen Gesichter
        int faceSize = 0; // minimale Groesse des zu findenden Gesichts in Pixeln
        ArrayList<Mat> croppedFaces = new ArrayList<>(); // extrahierte Gesichter
        int height; // Gesichtshoehe in Pixeln
        int pixelOffset = 100; // Rand um das gefundene Gesicht fuer Crop nach dem Rotieren

        // Bild normalisieren
        equalizeHist(image, image);

        height = image.rows();
        // minimale Groesse des Gesichts am Bild berechnen
        if (Math.round(height * 0.1f ) > 0)
        {
            faceSize = Math.round(height * 0.1f);
        }
        faceClassifier.detectMultiScale(image, faces, 1.1, 2, opencv_objdetect.CASCADE_SCALE_IMAGE,
                new Size(faceSize, faceSize), new Size());

        for (int i = 0; i < faces.size(); i++) {

            // lass fuer die Drehung einen pixelOffset breiten Rand um das gesicht stehen
            while (faces.get(i).x() - pixelOffset < 0 // x und y sind obere linke Ecke
                    || faces.get(i).y() - pixelOffset < 0
                    || faces.get(i).height() + faces.get(i).y() + pixelOffset > image.rows()
                    || faces.get(i).width() + faces.get(i).x() + pixelOffset > image.cols())
            {
                pixelOffset -= 10;
            }
            Rect offsetFace = new Rect(
                    faces.get(i).x() - pixelOffset,
                    faces.get(i).y() - pixelOffset,
                    faces.get(i).width() + 2*pixelOffset,
                    faces.get(i).height() + 2*pixelOffset);

            // Schneide Gesichter aus, rotiere sie, skaliere sie
            croppedFaces.add(rotateFace(image.apply(offsetFace), pixelOffset));
        }

        return croppedFaces;
    }

    /**
     * Methode zum vertikalen Ausrichten (Rotieren) von Gesichtern
     * @param image
     *   Bild, das rotiert werden soll.
     * @param borderCut
     *   Rand der nach der Rotation abgeschnitten wird um Artefakte zu vermeiden.
     * @return
     *   Ausgerichtetes (rotiertes) Bild.
     */
    private Mat rotateFace(Mat image, int borderCut) {
        Mat rotatedImage = new Mat(); // rotiertes Bild
        RectVector eyes = new RectVector(); // Augen
        Rect[] eyeRects; // Array der Augen
        Rect rightEye, leftEye; // linkes und rechtes Auge
        int neighbours = 1; // Anzahl an Nachbarn fuer erfolgreiche Detektion; siehe: http://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html
        int numberOfEyes; // Anzahl der Augen im Bild.
        int eyeSize = 0; // Minimale Augengroesse in Pixeln
        double rotationDegree; // Rotation des Gesichts im Bild
        // Bild ohne Rand
        Rect cutRect = new Rect(borderCut, borderCut, image.cols() - 2*borderCut, image.rows()- 2*borderCut);
        Size size = new Size(100,100); // Groesse, auf die das Bild letztendlich skaliert wird
        int height = image.rows();

        // minimale Groesse der Augen am Bild berechnen
        if (Math.round(height * 0.1f) > 0) {
            eyeSize = Math.round(height * 0.1f);
        }

        // Augen im Bild finden
        // Wenn mehr oder weniger als 2 Augen gefunden werden, versuche erneut Augen zu finden, aber mit erhoehter
        // Nachbarschaft (s.o.).
        do {
            eyeClassifier.detectMultiScale(image, eyes, 1.1, 4, opencv_objdetect.CASCADE_SCALE_IMAGE,
                    new Size(eyeSize, eyeSize), new Size());

            eyeRects = new Rect[(int)eyes.size()];
            for (int i = 0; i < eyes.size(); i++) {
                eyeRects[i] = eyes.get(i);
            }

            neighbours++;
            numberOfEyes = eyeRects.length;
        } while (numberOfEyes != 2 && neighbours < 3);

        // Wenn weniger als 2 Augen gefunden wurden, fuehre keine Rotation durch
        if (eyeRects.length < 2) {
            Mat resizedImage = new Mat();
            resize( image.apply(cutRect), resizedImage, size );
            return resizedImage;
        }

        // Lage der Augen bestimmen
        if (eyeRects[0].x() + eyeRects[0].width()/2 < eyeRects[1].x() + eyeRects[1].width()/2) {
            leftEye = eyeRects[0];
            rightEye = eyeRects[1];
        } else {
            leftEye = eyeRects[1];
            rightEye = eyeRects[0];
        }

        // Rotationswinkel der Augen bestimmen
        rotationDegree = Math.atan2(((rightEye.y() + rightEye.height()/2) - (leftEye.y() + leftEye.height()/2)),
                        ((rightEye.x() + rightEye.width()/2) - (leftEye.x() + leftEye.width()/2)) ) * 180 / Math.PI;

        // Wenn der Winkel zu gross sein sollte (zB. bei false positives bei der Augenerkennung)
        // bearbeite das Bild nicht weiter und returne es
        if (rotationDegree < -40 || rotationDegree > 40){
            Mat resizedImage = new Mat();
            resize( image.apply(cutRect), resizedImage, size );
            return resizedImage;
        }

        // Rotationspunkt bestimmen (Mitte des Bildes)
        Point2f pt = new Point2f(height/2, height/2);
        Mat rotationMatrix = getRotationMatrix2D(pt, rotationDegree, 1.0);

        // Bild rotieren
        warpAffine(image, rotatedImage, rotationMatrix, new Size(height, height));

        // Rand abschneiden
        Mat cutImage = rotatedImage.apply(cutRect);

        Mat resizedImage = new Mat();
        resize( cutImage, resizedImage, size );

        return resizedImage;
    }
}
