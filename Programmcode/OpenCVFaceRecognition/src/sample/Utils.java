package sample;

/*  JavaFX Imports  */
import javafx.scene.image.Image;

/*  OpenCV Imports  */
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.opencv_core.*;

import static org.bytedeco.javacpp.opencv_imgcodecs.imencode;

/*  Sonstige Imports  */
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/**
 * Behelfsklasse fuer die Bildumwandlung und andere Utility-Funktionen.
 * Created by Frederick on 08.08.2016.
 */
public class Utils {

    /**
     * Methode wandelt ein JavaCV (bytedeco) Mat in ein png Image um.
     * @param mat
     *   Mat, welche zu Bild umgewandelt werden soll.
     * @return
     *   Image aus der Mat.
     */
    public static Image mat2Image(Mat mat) {
        if (mat == null) mat = new Mat();
        BytePointer buffer = new BytePointer();
        imencode(".png", mat, buffer);
        return new Image(new ByteArrayInputStream(buffer.getStringBytes()));
    }

    /**
     * Uebersetzt eine ArrayList vom Typ Integer in einen Array vom Typ Integer.
     * @param al
     *   ArrayList vom Typ Integer.
     * @return
     *   Array vom Typ Integer.
     */
    public static int[] intArrayListToArray(ArrayList<Integer> al) {
        int[] array = new int[al.size()];

        for(int i = 0; i < al.size(); i++) {
            array[i] = al.get(i);
        }

        return array;
    }
}


/**
 * Klasse zum Speichern und Verwalten von face recognizer
 * prediction outcomes.
 */
class PredictionOutcome {
    private int correctLabel;
    private int predictedLabel;
    private boolean correct;
    private double distance;

    /**
     * Getter fuer das vorhergesagte Label.
     * @return
     *      Das vorhergesagte Label.
     */
    public int getPredictedLabel() {
        return predictedLabel;
    }

    /**
     * Getter fuer die vorhergesagte Distanz.
     * @return
     *      Distanz der Vorhersage.
     */
    public double getPredictedDistance() {
        return distance;
    }

    /**
     * Getter fuer das eigentlich korrekte Label.
     * @return
     *      Das Korrekte Label.
     */
    public int getCorrectLabel() {
        return correctLabel;
    }

    /**
     * Setter fuer das eigentlich korrekte Label.
     * @param label
     *      Das zu setzende Label.
     */
    public void setCorrectLabel(int label) {
        correctLabel = label;
        if (correctLabel == predictedLabel)
            correct = true;
    }

    /**
     * Getter ob die Vorhersage/die Labels korrekt sind.
     * @return
     *      True, wenn vorhergesagte ID und tatsaechliche ID uebereinstimmen.
     */
    public boolean isCorrect() {
        return correct;
    }

    /**
     * Methode zum Ausgeben des PredictionOutcomes als String.
     * @return
     *      PredictionOutcome als String.
     */
    public String toString() {
        return new String("P: " + predictedLabel
                + "    L: " + correctLabel + " (" + correct + ") " + "   C:" + distance);
    }

    /**
     * Konstruktor eines predictionOutcome Objekts. Container zum Speichern und Verarbeiten von face recognizer
     * prediction outcomes.
     * @param predictedLabel
     *      Precicted Label, vorhergesagtes Label.
     * @param distance
     *      Prediction Distance, Aehnlichkeit zur vorhergesagten Klasse.
     */
    public PredictionOutcome(int predictedLabel, double distance) {
        this.predictedLabel = predictedLabel;
        this.distance = distance;
        correct = false;
    }

}
