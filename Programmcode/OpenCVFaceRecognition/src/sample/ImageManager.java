package sample;

/*  JavaFX Imports  */
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/*  OpenCV Imports  */
import org.bytedeco.javacpp.opencv_core.Mat;

/*  Sonstige Imports  */
import java.util.ArrayList;
import java.util.Objects;

/**
 * Klasse zum Speichern und Verwalten von Bildern und Metainformationen.
 * Created by Frederick on 03.09.2016.
 */
public class ImageManager {
    private ArrayList<ImageDataContainer> data;
    private Integer errorID;
    private int maxPicturesPerPerson;

    /**
     * Ermittelt die gesamte Anzahl an Bildern von Personen mit mindestens minPictures Bildern.
     * @param minPictures
     *      Mindestanzahl an Bildern die eine Person haben muss um mitgezaehlt zu werden.
     * @return
     *      Anzahl an Bildern.
     */
    public int getTotalAmountOfPictures(int minPictures) {
        int total = 0;
        for (ImageDataContainer container : data) {
            if (data.size() >= minPictures) total+=container.getImages().size();
        }
        return total;
    }

    /**
     * Methode ermittelt die Anzahl an Personen die mindestens n Bilder besitzen.
     * @param amount
     *      Mindestanzahl an Bildern.
     * @return
     *      Anzahl der Personen/Containern mit mindestens 'amount' Bildern.
     */
    public int getAmountOfPeopleWithMorePicturesThan(int amount ) {
        if (amount <= 0) return data.size();
        int ret = 0;
        for (ImageDataContainer container : data) {
            if (container.getNumberOfImages() >= amount)
                ret++;
        }
        return ret;
    }

    /**
     * Methode zum Loeschen aller Daten im ImageManager.
     */
    public void clear() {
        data = new ArrayList<>();
        errorID = -1;
        maxPicturesPerPerson = 0;
    }

    /**
     * Methode zum Loeschen des Bildes und seiner Metainformationen mit dem Index index.
     * @param filename
     *      filename des zu entfernenden Bildes.
     */
    public void delete(String filename) {
        for (ImageDataContainer container : data) {
            container.remove(filename);
        }
    }

    /**
     * Methode zum Hinzufuegen eines Bildes und seiner Metainformationen.
     * @param mat
     *      Mat des Bildes.
     * @param id
     *      Fuer die Person eindeutige Personen ID des Bildes.
     * @param filename
     *      Dateiname des Bildes
     */
    public void add(Mat mat, Integer id, String filename) {
        for (ImageDataContainer container : data) {
            // Suche den Container mit der richtigen ID
            if (Objects.equals(container.getID(), id)) {
                container.add(mat, filename);
                // aktualisiere ggf maxPicturesPerPerson
                if (container.getNumberOfImages() > maxPicturesPerPerson)
                    maxPicturesPerPerson = container.getNumberOfImages();
                return;
            }
        }
        data.add(new ImageDataContainer(mat, filename, id));
    }

    /**
     * Methode zum Hinzufuegen eines Bildes und seiner Metainformationen.
     * @param mat
     *      Mat des Bildes.
     * @param idString
     *      Fuer die Person eindeutige Personen ID des Bildes.
     * @param filename
     *      Dateiname des Bildes
     */
    public void add(Mat mat, String idString, String filename) {
        int idInt;
        try {
            idInt = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            // Wenn die ID nicht richtig geparst werden konnte, nehme eine negative error-ID
            idInt = errorID;
            errorID--;
        }
        add(mat, idInt, filename);
    }

    /**
     * Methode, welche die groesste Anzahl an Bildern zurueckgibt die irgendeine Person hat.
     * @return
     *      Anzahl der Bilder.
     */
    public int getMaxPicturesPerPerson() {
        return maxPicturesPerPerson;
    }

    /**
     * Methode zum finden des zu einem Filename passenden Mat in allen ImageDataContainern.
     * @param filename
     *      Der Filename des gesuchten Mat.
     * @return
     *      Das Mat oder null wenn nicht vorhanden.
     */
    public Mat getMat(String filename) {
        for (ImageDataContainer container : data) {
            if (container.getFilenameList().contains(filename)) {
                return container.getImages().get(container.getFilenameList().indexOf(filename));
            }
        }
        return null;
    }

    /**
     * Methode zum ermitteln der zu einem Filename gehoerenden Container/Personen-ID.
     * @param filename
     *      Der Filename dessen ID gesucht wird.
     * @return
     *      Die ID als String.
     */
    public String getIDofFilenameAsString (String filename) {
        for (ImageDataContainer container : data) {
            if (container.getFilenameList().contains(filename)) {
                return ""+container.getID();
            }
        }
        return "ERROR";
    }

    /**
     * Methode welche eine Liste aller in allen Containern vorhandenen Filenames zurueckgibt.
     * @return
     *     Observable Array List aller Filenames.
     */
    public ObservableList<String> getFilenameList () {
        ObservableList<String> filenames = FXCollections.observableArrayList();
        for (ImageDataContainer container : data) {
            filenames.addAll(container.getFilenameList());
        }
        return filenames;
    }

    /**
     * Methode, welche die ersten 'count' Bilder jeder Person in einer Liste zurueck gibt.
     * @param count
     *      Anzahl der Bilder pro Person.
     * @return
     *      Liste der Bilder.
     */
    public ObservableList<String> getFilenameList (int count) {
        ObservableList<String> filenames = FXCollections.observableArrayList();
        for (ImageDataContainer container : data) {
            if (count != 0) {
                if (container.getNumberOfImages() >= count) {
                    for (int i = 0; i < count; i++) {
                        filenames.add(container.getFilenameList().get(i));
                    }

                }
            } else {
                filenames.addAll(container.getFilenameList());
            }
        }
        return filenames;
    }

    /**
     * Getter fuer den ImageDataContainer Array.
     * @return
     *      Der ImageDataContainer Array
     */
    public ArrayList<ImageDataContainer> getData () {
        return data;
    }

    /**
     * Konstruktor des Image Managers.
     */
    public ImageManager () {
        data = new ArrayList<>();
        errorID = -1;
        maxPicturesPerPerson = 0;
    }
}
