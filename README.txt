==== Zugangsdaten VM ====
Benutzername: user
Passwort: passwort

==== Programmstart über IntelliJ IDEA ====
Zum Starten des OpenCV Face Recognizers doppelklicken sie auf die Verknüpfung zu IntelliJ IDEA.
Sollte IntelliJ IDEA nach einem Projekt fragen, wählen sie OpenCVFaceRecognizer.
Wenn IntelliJ IDEA geöffnet ist, klicken sie in der oberen rechten Ecke auf das grüne Dreieck (Play-Symbol).
Nun können sie das Programm uneingeschränkt benutzen.

==== Programmstart über .jar Datei ====
Gehen sie sicher, dass sowohl Java als auch OpenCV auf ihrem PC installiert sind.
Öffnen sie ein Konsolenfenster.
Navigieren sie in den Ordner mit der OpenCVFaceRecognizer.jar Datei.
Starten sie sie das Programm über
	java -jar OpenCVFaceRecognizer.jar

==== Programmbedienung ====
(1) Zum Laden von Trainingsbildern, klicken sie auf "Open Directory" auf der linken Seite der GUI.
	(1a) Selektieren sie vorher "Crop On Open" um die Bilder passend zuzuschneiden. Dies ist bei noch nicht zugeschnittenen Bildern immer erforderlich! 
	(1b) Sollten sie die zugeschnittenen Bilder für die spätere Verwendung speichern wollen, klicken sie auf "Save Images".
(2) Wählen sie in der Mitte mit den Radio Buttons den zu verwendenden Algorithmus.
(3) Wählen sie mit dem Drop Down Menü in der Mitte die Anzahl der Bilder, mit der jede Person trainiert werden soll. Personen mit weniger Bildern als angegeben, werden nicht trainiert.
(4) Klicken sie auf "Train Recognizer" und warten sie bis sich der darunter stehende Status auf "Trained" ändert.
(5) Klicken sie auf der rechten Seite der GUI auf "Open Directory" um die Testbilder zu laden. Diese müssen nicht zugeschnitten sein.
(6) Klicken sue auf "Recognize". Nach Ablauf des Fortschrittsbalkens wird das Ergebnisfenster geöffnet.
